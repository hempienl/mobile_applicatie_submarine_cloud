import 'package:asd/models/node.dart';

abstract class NodeInformation {
  Future<List<Node>> getNodeInfo();
}
