import 'dart:typed_data';
import 'package:encrypt/encrypt.dart';
import 'package:pointycastle/api.dart';

abstract class Encryption {
  Uint8List encryptAsymmetric(Uint8List contentToEncrypt, PublicKey publicKey);
  Uint8List encryptSymmetric(Uint8List contentToEncrypt, Key symmetricKey);
}