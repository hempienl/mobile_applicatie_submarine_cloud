import 'package:encrypt/encrypt.dart';
import 'package:pointycastle/api.dart';

abstract class KeyGen {
  AsymmetricKeyPair<PublicKey, PrivateKey> generateAsymmetricKeyPair();
  Key generateSymmetricKey();
}