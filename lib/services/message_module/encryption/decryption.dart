import 'dart:typed_data';
import 'package:encrypt/encrypt.dart';
import 'package:pointycastle/api.dart';

abstract class Decryption {
   Uint8List decryptAsymmetric(Uint8List contentToDecrypt,
       PrivateKey privateKey);
   Uint8List decryptSymmetric(Uint8List contentToDecrypt, Key symmetricKey);
}