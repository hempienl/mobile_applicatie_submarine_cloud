import 'package:asd/models/message.dart';

abstract class PersistMessages {
  Future<void> insertMessage(int conversationId, Message message);
  Future<List<Message>> getMessageList(int conversationId);
}