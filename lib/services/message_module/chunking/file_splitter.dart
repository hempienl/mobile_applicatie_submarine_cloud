import 'dart:io';

abstract class FileSplitter {
  Future<List<File>> chunkFile(File file);
  File mergeFile(List<File> files, File destination);
}