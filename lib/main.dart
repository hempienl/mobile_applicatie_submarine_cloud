import 'package:asd/route_generator.dart';
import 'package:asd/themes/theme.dart';
import 'package:flutter/material.dart';

//test push to
void main() => runApp(Main());

class Main extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Confidential Messenger',
      theme: MainTheme,
      initialRoute: '/',
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}
